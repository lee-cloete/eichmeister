<?php
/**
 * Template Name: Custom Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <section id="gallery">
    <div class="container">
      <div class="row">


        <div class="col-md-6">
          <figure class="figure">
              <img src="<?php site_url() ?>/wp-content/uploads/2019/07/aerial-architectural-design-architecture-373912-1-e1562523234866.jpg" class="figure-img img-fluid" alt=".">
            <figcaption class="figure-caption text-right">
              <h1>Welcher Speed Passt</h1>
              <p>Der richtige fur ihre Familie</p>
            </figcaption>
          </figure>
        </div>

        <div class="col-md-6">
          <figure class="figure">
              <img src="<?php site_url() ?>/wp-content/uploads/2019/07/amphitheater-arches-architecture-1797161-1-e1562523388256.jpg" class="figure-img img-fluid img-h-c" alt=".">
              <figcaption class="figure-caption text-right">
                <h1>Internet Fur Grossfamilien</h1>
                <p>Passen Sie ihr internet zu ihren Bedürfnissen</p>
              </figcaption>
          </figure>
 
          <figure class="figure">
              <img src="<?php site_url() ?>/wp-content/uploads/2019/07/bright-ecology-environment-1006115-1-e1562523412259.jpg" class="figure-img img-fluid img-h-c" alt=".">
              <figcaption class="figure-caption text-right">
                <h1>Sicherheitsfunktionen</h1> 
                <p>Von der Firewall bis zur Kindersicherung</p>
              </figcaption>
          </figure>
        </div>


        <div>
    </div>
</section>

<section id="search">

    <div class="container holder">
    
      <div class="row">
      
      
        <div class="col-md-4 spacer-text-left">
            <h1>Verfugbarkeits - Check</h1>
            <p> Leistung an dieser Adresse prüfen : </p>
        </div>

        <div class="col-md-5">

          <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link  active" href="#profile" role="tab" data-toggle="tab" aria-selected="true"><img src="http://eichmeister.local/wp-content/uploads/2019/07/map.png"></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#buzz" role="tab" data-toggle="tab"><img src="http://eichmeister.local/wp-content/uploads/2019/07/oto.png"></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#references" role="tab" data-toggle="tab"><img src="http://eichmeister.local/wp-content/uploads/2019/07/phone.png"></a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="profile">
              <form>
                <div class="form-row">
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="Strasse">
                  </div>
                  <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Nr">
                  </div>
                </div>

                <div class="form-row">
                  <div class="col-md-5">
                    <input type="text" class="form-control" placeholder="PLZ">
                  </div>
                  <div class="col-md-7">
                    <input type="text" class="form-control" placeholder="Ort">
                  </div>
                </div>

              </form>              
              </div>
              <div role="tabpanel" class="tab-pane fade" id="buzz">
              <p>ID ihrer OTO-Wandose </p>
              <div class="form-row">
                  <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="B110.100.100.1">
                  </div>
                </div>

                </div>
              <div role="tabpanel" class="tab-pane fade" id="references">
              <p>Festnetz-Nummer an diesem Anschluss</p>
              <div class="form-row">
                  <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="099 999 99 99">
                  </div>
                </div>
                </div>
            </div>

        </div>

        <div class="col-md-3 spacer-text-right">
          <button> Jetzt prüfen </button>
        </div>

        <div>

        <div>
    </div>

</section>


<?php endwhile; ?>
